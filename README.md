# EasyRecyclerView
This is an Easy RecyclerView library like the easy implementation of Horizontal and Vertical RecyclerView with some added hands-on features to simplify your workflow.

## gradle [![](https://jitpack.io/v/org.bitbucket.thinkwik/easyrecyclerview.svg)](https://jitpack.io/#org.bitbucket.thinkwik/easyrecyclerview)
```groovy
implementation 'org.bitbucket.thinkwik:easyrecyclerview:x.x.x'
```

## Are these easy?
```
// Add/Remove Head View (multiple supports)
mRecyclerView.addHeaderView() and .removeHeaderView()

// Add/Remove Bottom View (multiple supports)
mRecyclerView.addFooterView() and .removeFooterView()

// Set the split line (you can also specify the split line Divider and split line size directly in the layout file, of course you can also use your own split line)
mRecyclerView.setDivider() If it's a grid or waterfall view, you can even set the split line Divider and split line size

/ / Set the data empty View (set isRetainShowHeadOrFoot to true, you can not clear the added HeadView and FooterView when displaying EmptyView)
mRecyclerView.setEmptyView()

// Item click event
mRecyclerView.setOnItemClickListener(new com.thinkwik.rec.OnItemClickListener() {
    @Override
    Public void onItemClick(EasyRecyclerView easyRecyclerView, View view, int position) {
        // ...
    }
});

// Item long press event
mRecyclerView.setOnItemLongClickListener(new com.thinkwik.rec.OnItemLongClickListener() {
    @Override
    Public boolean onItemLongClick(EasyRecyclerView easyRecyclerView, View view, int position) {
        Return true;
    }
});

// Set the event callback when scrolling to the top or bottom
mRecyclerView.setOnScrollListener(new EasyRecyclerViewOnScrollListener(mRecyclerView.getLayoutManager()) {
    @Override
    Public void onScrolledToTop() {
        // top
    }

    @Override
    Public void onScrolledToBottom() {
        // bottom
    }
});
```


Also added
Easy Adapter [An additional Adapter for DataBinding] (https://bitbucket.org/thinkwik/easyrecyclerview/src/master/app/src/main/java/com/thinkwik/easyrecyclerviewdemo/DataBindingAdapter.java)
```
mAdapter = new EasyAdapter<UserInfo>(this, R.layout.item_view, data) {
    @Override
    Public void onBindViewHolder(ViewHolder holder, int position) {
        TextView tvName = holder.findView(R.id.tv_name);
        tvName.setText(mAdapter.getData(position).getName());
    }
};

Wait...
Of course, all of the original RecyclerView can be used normally.
```

### Layout
```
// LinearLayout (ListView)
<com.thinkwik.rec.EasyRecyclerView
    Android:id="@+id/mRecyclerView"
    Android:layout_width="match_parent"
    Android:layout_height="match_parent"
    Android:scrollbars="vertical"
    App:easy_divider="#696969"
    App:easy_dividerHeight="1dp"
    App:easy_emptyView="@id/tv_empty"
    App:easy_layoutManager="linear"
    App:easy_layoutManagerOrientation="vertical" />
    
// GridLayout (GridView)
<com.thinkwik.rec.EasyRecyclerView
    Android:id="@+id/mRecyclerView"
    Android:layout_width="match_parent"
    Android:layout_height="match_parent"
    Android:scrollbars="vertical"
    App:easy_dividerHorizontal="#FFEE00"
    App:easy_dividerVertical="#FFCCDD"
    App:easy_dividerHorizontalHeight="10dp"
    App:easy_dividerVerticalHeight="30dp"
    App:easy_itemViewBothSidesMargin="20dp"
    App:easy_layoutManager="grid"
    App:easy_layoutManagerOrientation="vertical"
    App:easy_spanCount="3" />
    
// StaggeredGridLayout (waterfall stream)
<com.thinkwik.rec.EasyRecyclerView
      Android:id="@+id/mRecyclerView"
      Android:layout_width="match_parent"
      Android:layout_height="match_parent"
      Android:scrollbars="vertical"
      App:easy_divider="#EFADEF"
      App:easy_dividerHorizontalHeight="10dp"
      App:easy_dividerVerticalHeight="10dp"
      App:easy_itemViewBothSidesMargin="20dp"
      App:easy_layoutManager="staggeredGrid"
      App:easy_layoutManagerOrientation="vertical"
      App:easy_spanCount="2" />
```

### Pulldown Refresh + Load More
Beginning with version 1.3.0, EasyRefreshRecyclerView has been added to implement pull-down refresh and load more
[Recommended sample code] (https://bitbucket.org/thinkwik/easyrecyclerview/src/master/app/src/main/java/com/thinkwik/easyrecyclerviewdemo/ImitateNewListViewDemoActivity.java)
```
/ / Layout (EasyRecyclerView properties can be used, List, Gird, staggeredGrid can be set)
<com.thinkwik.rec.EasyRefreshRecyclerView
    Android:id="@+id/cv_refreshListRecyclerView"
    Android:layout_width="match_parent"
    Android:layout_height="match_parent"
    Android:scrollbars="vertical"
    App:easy_divider="#333333"
    App:easy_dividerHeight="0.5dp"
    App:easy_emptyView="@id/tv_empty"
    App:easy_isEmptyViewKeepShowHeadOrFooter="true"
    App:easy_layoutManager="linear"
    App:easy_layoutManagerOrientation="vertical" />

// pulldown refresh callback
mCvRefreshListRecyclerView.setOnPullRefreshListener(...)

// load more callbacks
mCvRefreshListRecyclerView.setOnLoadMoreListener(...)

/ / Set to load more View
mCvRefreshListRecyclerView.setLoadMoreView(...)

/ / Set start / disable pull-down refresh
mCvRefreshListRecyclerView.setLoadMoreEnabled(true / false);

// set start/disable loading more
mCvRefreshListRecyclerView.setPullRefreshEnabled(true / false);
```

### Custom Configuration
| Parameter | Type | Default | Description|
|--- | --- | ---| ---|
|easy_divider | reference / color | none | global split line divider|
|easy_dividerVertical | reference / color | none | vertical split line divider|
|easy_dividerHorizontal | reference / color | none | horizontal split line divider|
|easy_dividerHeight | dimension | 1px | global split line size|
|easy_dividerVerticalHeight | dimension | 1px | Vertical Split Line size|
|easy_dividerHorizontalHeight | dimension | 1px | horizontal split line size|
|easy_dividerHorizontalLeftMargin | dimension | none | horizontal dividing line left margin |
|easy_dividerHorizontalRightMargin | dimension | none | horizontal split line right margin |
|easy_dividerVerticalTopMargin | dimension | None | Vertical Split Line Margin |
|easy_dividerVerticalBottomMargin | dimension | none | vertical split line bottom margin |
|easy_isNotShowGridEndDivider | boolean | false | Whether to display the split line of the last item of the Grid|
|easy_itemViewBothSidesMargin | dimension | None | Margins on both sides of the itemView (the sides of the headerView and footerView are not set) |
|easy_emptyView | reference | none | emptyView id|
|easy_isEmptyViewKeepShowHeadOrFooter | boolean | false | Whether to keep the displayed HeadView and FooterView when the EmptyView is displayed
|easy_layoutManager | linear / grid / staggeredGrid | linear | Layout Type |
|easy_layoutManagerOrientation| horizontal / vertical | vertical | Layout Direction |
|easy_spanCount | integer | 2 | Number of grids, valid when easy_layoutManager=grid / staggeredGrid |
|easy_headerDividersEnabled | boolean | false | Whether to enable split lines in headView |
|easy_footerDividersEnabled | boolean | false | Whether to enable split lines in footerView |