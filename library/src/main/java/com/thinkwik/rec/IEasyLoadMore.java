package com.thinkwik.rec;

import android.view.View;

/**
 * LoadMoreView interface
 * Created by Thinkwik 16/04/13.

 */
public interface IEasyLoadMore {

    void showLoading();

    void showNormal();

    boolean isLoading();

    View getView();

}
