package com.thinkwik.rec;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import cn.iwgang.easyrecyclerview.R;

/**
 * EasyDefaultLoadMoreView
 * Created by Thinkwik 17/1/2.

 */
class EasyDefaultLoadMoreView extends FrameLayout implements IEasyLoadMore {
    private ProgressBar mPbLoad;
    private TextView mTvLoadText;

    private boolean isLoading = false;

    public EasyDefaultLoadMoreView(Context context) {
        this(context, null);
    }

    public EasyDefaultLoadMoreView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public EasyDefaultLoadMoreView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView();
    }

    private void initView() {
        inflate(getContext(), R.layout.easy_view_def_load_more, this);
        mPbLoad = (ProgressBar) findViewById(R.id.easy_pbLoad);
        mTvLoadText = (TextView) findViewById(R.id.easy_tvLoadText);
    }

    @Override
    public View getView() {
        return this;
    }

    @Override
    public void showNormal() {
        isLoading = false;
        mPbLoad.setVisibility(GONE);
        mTvLoadText.setText(getResources().getString(R.string.easy_def_load_more_view_status_normal));
    }

    @Override
    public void showLoading() {
        isLoading = true;
        mPbLoad.setVisibility(VISIBLE);
        mTvLoadText.setText(getResources().getString(R.string.easy_def_load_more_view_status_loading));
    }

    @Override
    public boolean isLoading() {
        return isLoading;
    }

}