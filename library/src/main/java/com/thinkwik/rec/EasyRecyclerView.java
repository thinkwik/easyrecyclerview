package com.thinkwik.rec;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.OrientationHelper;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;

import java.util.ArrayList;
import java.util.List;

import cn.iwgang.easyrecyclerview.R;

/**
 * EasyRecyclerView
 * Created by Thinkwik 15/10/31.

 */
public class EasyRecyclerView extends RecyclerView {
    public static final int LAYOUT_MANAGER_TYPE_LINEAR = 0;
    public static final int LAYOUT_MANAGER_TYPE_GRID = 1;
    public static final int LAYOUT_MANAGER_TYPE_STAGGERED_GRID = 2;

    private static final int DEF_LAYOUT_MANAGER_TYPE = LAYOUT_MANAGER_TYPE_LINEAR;
    private static final int DEF_GRID_SPAN_COUNT = 2;
    private static final int DEF_LAYOUT_MANAGER_ORIENTATION = OrientationHelper.VERTICAL;
    private static final int DEF_DIVIDER_HEIGHT = 30;

    private List<View> mHeaderView = new ArrayList<>();
    private List<View> mFooterView = new ArrayList<>();
    private EasyWrapRecyclerViewAdapter mWrapEasyRecyclerViewAdapter;
    private RecyclerView.Adapter mReqAdapter;
    private GridLayoutManager mCurGridLayoutManager;
    private EasyDefaultItemDecoration mEasyDefaultItemDecoration;

    private Drawable mVerticalDivider;
    private Drawable mHorizontalDivider;
    private int mVerticalDividerHeight;
    private int mHorizontalDividerHeight;
    private int mHorizontalDividerLeftMargin;
    private int mHorizontalDividerRightMargin;
    private int mVerticalDividerTopMargin;
    private int mVerticalDividerBottomMargin;
    private int mItemViewBothSidesMargin;
    private boolean isHeaderDividersEnabled = false;
    private boolean isFooterDividersEnabled = false;
    private boolean isDefaultItemDecoration = true;
    private boolean isKeepShowHeadOrFooter = false;
    private boolean isNotShowGridEndDivider = false;
    private int mEmptyViewResId;
    private View mEmptyView;
    private OnItemClickListener mTempOnItemClickListener;
    private OnItemLongClickListener mTempOnItemLongClickListener;
    private OnHeadViewBindViewHolderListener mTempOnHeadViewBindViewHolderListener;
    private OnFooterViewBindViewHolderListener mTempOnFooterViewBindViewHolderListener;
    private int mLayoutManagerType;
    private Drawable mDefAllDivider;
    private int mDefAllDividerHeight;
    private boolean needInitAddItemDescration = false;
    private boolean hasShowEmptyView = false;

    public EasyRecyclerView(Context context) {
        this(context, null);
    }

    public EasyRecyclerView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public EasyRecyclerView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(context, attrs);
    }

    private void init(Context context, AttributeSet attrs) {
        TypedArray ta = context.obtainStyledAttributes(attrs, R.styleable.EasyRecyclerView);
        mDefAllDivider = ta.getDrawable(R.styleable.EasyRecyclerView_easy_divider);
        mDefAllDividerHeight = (int) ta.getDimension(R.styleable.EasyRecyclerView_easy_dividerHeight, -1);
        mVerticalDivider = ta.getDrawable(R.styleable.EasyRecyclerView_easy_dividerVertical);
        mHorizontalDivider = ta.getDrawable(R.styleable.EasyRecyclerView_easy_dividerHorizontal);
        mVerticalDividerHeight = (int) ta.getDimension(R.styleable.EasyRecyclerView_easy_dividerVerticalHeight, -1);
        mHorizontalDividerHeight = (int) ta.getDimension(R.styleable.EasyRecyclerView_easy_dividerHorizontalHeight, -1);
        mHorizontalDividerLeftMargin = (int) ta.getDimension(R.styleable.EasyRecyclerView_easy_dividerHorizontalLeftMargin, 0);
        mHorizontalDividerRightMargin = (int) ta.getDimension(R.styleable.EasyRecyclerView_easy_dividerHorizontalRightMargin, 0);
        mVerticalDividerTopMargin = (int) ta.getDimension(R.styleable.EasyRecyclerView_easy_dividerVerticalTopMargin, 0);
        mVerticalDividerBottomMargin = (int) ta.getDimension(R.styleable.EasyRecyclerView_easy_dividerVerticalBottomMargin, 0);
        mItemViewBothSidesMargin = (int) ta.getDimension(R.styleable.EasyRecyclerView_easy_itemViewBothSidesMargin, 0);
        mEmptyViewResId = ta.getResourceId(R.styleable.EasyRecyclerView_easy_emptyView, -1);
        isKeepShowHeadOrFooter = ta.getBoolean(R.styleable.EasyRecyclerView_easy_isEmptyViewKeepShowHeadOrFooter, false);
        isHeaderDividersEnabled = ta.getBoolean(R.styleable.EasyRecyclerView_easy_headerDividersEnabled, false);
        isFooterDividersEnabled = ta.getBoolean(R.styleable.EasyRecyclerView_easy_footerDividersEnabled, false);
        isNotShowGridEndDivider = ta.getBoolean(R.styleable.EasyRecyclerView_easy_isNotShowGridEndDivider, false);
        int layoutManagerType = ta.getInt(R.styleable.EasyRecyclerView_easy_layoutManager, DEF_LAYOUT_MANAGER_TYPE);
        int layoutManagerOrientation = ta.getInt(R.styleable.EasyRecyclerView_easy_layoutManagerOrientation, DEF_LAYOUT_MANAGER_ORIENTATION);
        boolean isReverseLayout = ta.getBoolean(R.styleable.EasyRecyclerView_easy_isReverseLayout, false);
        int gridSpanCount = ta.getInt(R.styleable.EasyRecyclerView_easy_spanCount, DEF_GRID_SPAN_COUNT);

        switch (layoutManagerType) {
            case LAYOUT_MANAGER_TYPE_LINEAR:
                setLayoutManager(new LinearLayoutManager(context, layoutManagerOrientation, isReverseLayout));
                break;
            case LAYOUT_MANAGER_TYPE_GRID:
                setLayoutManager(new GridLayoutManager(context, gridSpanCount, layoutManagerOrientation, isReverseLayout));
                break;
            case LAYOUT_MANAGER_TYPE_STAGGERED_GRID:
                setLayoutManager(new StaggeredGridLayoutManager(gridSpanCount, layoutManagerOrientation));
                break;
        }
        ta.recycle();
    }

    private void processDefDivider(boolean isLinearLayoutManager, int layoutManagerOrientation) {
        if (!isDefaultItemDecoration) return;

        if ((null == mVerticalDivider || null == mHorizontalDivider) && null != mDefAllDivider) {
            if (isLinearLayoutManager) {
                if (layoutManagerOrientation == OrientationHelper.VERTICAL && null == mHorizontalDivider) {
                    mHorizontalDivider = mDefAllDivider;
                } else if (layoutManagerOrientation == OrientationHelper.HORIZONTAL && null == mVerticalDivider) {
                    mVerticalDivider = mDefAllDivider;
                }
            } else {
                if (null == mVerticalDivider) {
                    mVerticalDivider = mDefAllDivider;
                }

                if (null == mHorizontalDivider) {
                    mHorizontalDivider = mDefAllDivider;
                }
            }
        }

        if (mVerticalDividerHeight > 0 && mHorizontalDividerHeight > 0) return;

        if (mDefAllDividerHeight > 0) {
            if (isLinearLayoutManager) {
                if (layoutManagerOrientation == OrientationHelper.VERTICAL && mHorizontalDividerHeight <= 0) {
                    mHorizontalDividerHeight = mDefAllDividerHeight;
                } else if (layoutManagerOrientation == OrientationHelper.HORIZONTAL && mVerticalDividerHeight <= 0) {
                    mVerticalDividerHeight = mDefAllDividerHeight;
                }
            } else {
                if (mVerticalDividerHeight <= 0) {
                    mVerticalDividerHeight = mDefAllDividerHeight;
                }

                if (mHorizontalDividerHeight <= 0) {
                    mHorizontalDividerHeight = mDefAllDividerHeight;
                }
            }
        } else {
            if (isLinearLayoutManager) {
                if (layoutManagerOrientation == OrientationHelper.VERTICAL && mHorizontalDividerHeight <= 0) {
                    if (null != mHorizontalDivider) {
                        if (mHorizontalDivider.getIntrinsicHeight() > 0) {
                            mHorizontalDividerHeight = mHorizontalDivider.getIntrinsicHeight();
                        } else {
                            mHorizontalDividerHeight = DEF_DIVIDER_HEIGHT;
                        }
                    }
                } else if (layoutManagerOrientation == OrientationHelper.HORIZONTAL && mVerticalDividerHeight <= 0) {
                    if (null != mVerticalDivider) {
                        if (mVerticalDivider.getIntrinsicHeight() > 0) {
                            mVerticalDividerHeight = mVerticalDivider.getIntrinsicHeight();
                        } else {
                            mVerticalDividerHeight = DEF_DIVIDER_HEIGHT;
                        }
                    }
                }
            } else {
                if (mVerticalDividerHeight <= 0 && null != mVerticalDivider) {
                    if (mVerticalDivider.getIntrinsicHeight() > 0) {
                        mVerticalDividerHeight = mVerticalDivider.getIntrinsicHeight();
                    } else {
                        mVerticalDividerHeight = DEF_DIVIDER_HEIGHT;
                    }
                }

                if (mHorizontalDividerHeight <= 0 && null != mHorizontalDivider) {
                    if (mHorizontalDivider.getIntrinsicHeight() > 0) {
                        mHorizontalDividerHeight = mHorizontalDivider.getIntrinsicHeight();
                    } else {
                        mHorizontalDividerHeight = DEF_DIVIDER_HEIGHT;
                    }
                }
            }
        }
    }

    @Override
    public void setAdapter(Adapter adapter) {
        // Only once
        if (mEmptyViewResId != -1) {
            if (null != getParent()) {
                ViewGroup parentView = ((ViewGroup) getParent());
                View tempEmptyView1 = parentView.findViewById(mEmptyViewResId);

                if (null != tempEmptyView1) {
                    mEmptyView = tempEmptyView1;

                    if (isKeepShowHeadOrFooter) parentView.removeView(tempEmptyView1);
                } else {
                    ViewParent pParentView = parentView.getParent();
                    if (pParentView instanceof ViewGroup) {
                        View tempEmptyView2 = ((ViewGroup) pParentView).findViewById(mEmptyViewResId);
                        if (null != tempEmptyView2) {
                            mEmptyView = tempEmptyView2;

                            if (isKeepShowHeadOrFooter)
                                ((ViewGroup) pParentView).removeView(tempEmptyView2);
                        }
                    }
                }
            }
            mEmptyViewResId = -1;
        } else if (isKeepShowHeadOrFooter && null != mEmptyView) {
            ViewParent emptyViewParent = mEmptyView.getParent();
            if (emptyViewParent instanceof ViewGroup) {
                ((ViewGroup) emptyViewParent).removeView(mEmptyView);
            }
        }

        if (null == adapter) {
            if (null != mReqAdapter) {
                if (!isKeepShowHeadOrFooter) {
                    mReqAdapter.unregisterAdapterDataObserver(mReqAdapterDataObserver);
                }
                mReqAdapter = null;
                mWrapEasyRecyclerViewAdapter = null;

                processEmptyView();
            }

            return;
        }

        mReqAdapter = adapter;
        mWrapEasyRecyclerViewAdapter = new EasyWrapRecyclerViewAdapter(this, adapter, mHeaderView, mFooterView, mLayoutManagerType);

        mWrapEasyRecyclerViewAdapter.setOnItemClickListener(mTempOnItemClickListener);
        mWrapEasyRecyclerViewAdapter.setOnItemLongClickListener(mTempOnItemLongClickListener);
        mWrapEasyRecyclerViewAdapter.setOnHeadViewBindViewHolderListener(mTempOnHeadViewBindViewHolderListener);
        mWrapEasyRecyclerViewAdapter.setOnFooterViewBindViewHolderListener(mTempOnFooterViewBindViewHolderListener);

        mReqAdapter.registerAdapterDataObserver(mReqAdapterDataObserver);
        super.setAdapter(mWrapEasyRecyclerViewAdapter);

        if (needInitAddItemDescration && null != mEasyDefaultItemDecoration) {
            needInitAddItemDescration = false;
            super.addItemDecoration(mEasyDefaultItemDecoration);
        }

        processEmptyView();
    }

    public void reRegisterAdapterDataObserver() {
        if (null != mReqAdapter && !mReqAdapter.hasObservers()) {
            mReqAdapter.registerAdapterDataObserver(mReqAdapterDataObserver);
        }
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        if (null != mReqAdapter && mReqAdapter.hasObservers()) {
            mReqAdapter.unregisterAdapterDataObserver(mReqAdapterDataObserver);
        }
    }

    @Override
    public void setLayoutManager(LayoutManager layout) {
        super.setLayoutManager(layout);

        if (null == layout) return;

        if (layout instanceof GridLayoutManager) {
            mCurGridLayoutManager = ((GridLayoutManager) layout);
            mCurGridLayoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
                @Override
                public int getSpanSize(int position) {
                    if (position < getHeaderViewsCount() || position >= mReqAdapter.getItemCount() + getHeaderViewsCount()) {
                        // header or footer span
                        return mCurGridLayoutManager.getSpanCount();
                    } else {
                        // default item span
                        return 1;
                    }
                }
            });

            mLayoutManagerType = LAYOUT_MANAGER_TYPE_GRID;
            processDefDivider(false, mCurGridLayoutManager.getOrientation());
            initDefaultItemDecoration();
        } else if (layout instanceof StaggeredGridLayoutManager) {
            mLayoutManagerType = LAYOUT_MANAGER_TYPE_STAGGERED_GRID;
            processDefDivider(false, ((StaggeredGridLayoutManager) layout).getOrientation());
            initDefaultItemDecoration();
        } else if (layout instanceof LinearLayoutManager) {
            mLayoutManagerType = LAYOUT_MANAGER_TYPE_LINEAR;
            processDefDivider(true, ((LinearLayoutManager) layout).getOrientation());
            initDefaultItemDecoration();
        }
    }

    private void initDefaultItemDecoration() {
        if (!isDefaultItemDecoration) return;

        if (null != mEasyDefaultItemDecoration) {
            super.removeItemDecoration(mEasyDefaultItemDecoration);
            mEasyDefaultItemDecoration = null;
        }

        mEasyDefaultItemDecoration = new EasyDefaultItemDecoration(this, mVerticalDivider, mHorizontalDivider,
                mVerticalDividerHeight, mHorizontalDividerHeight,
                mHorizontalDividerLeftMargin, mHorizontalDividerRightMargin, mVerticalDividerTopMargin, mVerticalDividerBottomMargin);
        mEasyDefaultItemDecoration.setItemViewBothSidesMargin(mItemViewBothSidesMargin);
        mEasyDefaultItemDecoration.setHeaderDividersEnabled(isHeaderDividersEnabled);
        mEasyDefaultItemDecoration.setFooterDividersEnabled(isFooterDividersEnabled);
        mEasyDefaultItemDecoration.setNotShowGridEndDivider(isNotShowGridEndDivider);

        if (null != getAdapter()) {
            needInitAddItemDescration = false;
            super.addItemDecoration(mEasyDefaultItemDecoration);
        } else {
            needInitAddItemDescration = true;
        }
    }

    private void processEmptyView() {
        if (null != mEmptyView) {
            boolean isShowEmptyView = (null != mReqAdapter ? mReqAdapter.getItemCount() : 0) == 0;

            if (isShowEmptyView == hasShowEmptyView) return;

            if (isKeepShowHeadOrFooter) {
                if (hasShowEmptyView) {
                    mWrapEasyRecyclerViewAdapter.notifyItemRemoved(getHeaderViewsCount());
                }
            } else {
                mEmptyView.setVisibility(isShowEmptyView ? VISIBLE : GONE);
                setVisibility(isShowEmptyView ? GONE : VISIBLE);
            }

            hasShowEmptyView = isShowEmptyView;
        }
    }

    /**
     * Set EmptyView (before setAdapter)
     *
     * @param emptyView your EmptyView
     */
    public void setEmptyView(View emptyView) {
        setEmptyView(emptyView, false);
    }

    /**
     * Set EmptyView (before setAdapter)
     *
     * @param emptyView              your EmptyView
     * @param isKeepShowHeadOrFooter is Keep show HeadView or FooterView
     */
    public void setEmptyView(View emptyView, boolean isKeepShowHeadOrFooter) {
        mEmptyView = emptyView;
        this.isKeepShowHeadOrFooter = isKeepShowHeadOrFooter;
    }

    public View getEmptyView() {
        return mEmptyView;
    }

    public void setEmptyViewKeepShowHeadOrFooter(boolean isKeepShowHeadOrFoot) {
        this.isKeepShowHeadOrFooter = isKeepShowHeadOrFoot;
    }

    public boolean isShowEmptyView() {
        return hasShowEmptyView;
    }

    public boolean isKeepShowHeadOrFooter() {
        return isKeepShowHeadOrFooter;
    }

    public void setDivider(int height, Drawable divider) {
        if (!isDefaultItemDecoration || height <= 0) return;

        mVerticalDividerHeight = height;
        mHorizontalDividerHeight = height;

        if (mVerticalDivider != divider) {
            mVerticalDivider = divider;
        }

        if (mHorizontalDivider != divider) {
            mHorizontalDivider = divider;
        }

        if (null != mEasyDefaultItemDecoration) {
            mEasyDefaultItemDecoration.setVerticalDividerDrawableHeight(mVerticalDividerHeight);
            mEasyDefaultItemDecoration.setHorizontalDividerDrawableHeight(mHorizontalDividerHeight);

            mEasyDefaultItemDecoration.setVerticalDividerDrawable(mVerticalDivider);
            mEasyDefaultItemDecoration.setHorizontalDividerDrawable(mHorizontalDivider);

            if (null != mWrapEasyRecyclerViewAdapter) {
                mWrapEasyRecyclerViewAdapter.notifyDataSetChanged();
            }
        }
    }

    public void setDivider(Drawable divider) {
        if (!isDefaultItemDecoration || (mVerticalDividerHeight <= 0 && mHorizontalDividerHeight <= 0))
            return;

        if (mVerticalDivider != divider) {
            mVerticalDivider = divider;
        }

        if (mHorizontalDivider != divider) {
            mHorizontalDivider = divider;
        }

        if (null != mEasyDefaultItemDecoration) {
            mEasyDefaultItemDecoration.setVerticalDividerDrawable(mVerticalDivider);
            mEasyDefaultItemDecoration.setHorizontalDividerDrawable(mHorizontalDivider);

            if (null != mWrapEasyRecyclerViewAdapter) {
                mWrapEasyRecyclerViewAdapter.notifyDataSetChanged();
            }
        }
    }

    public void setDivider(Drawable dividerVertical, Drawable dividerHorizontal) {
        if (!isDefaultItemDecoration || (mVerticalDividerHeight <= 0 && mHorizontalDividerHeight <= 0))
            return;

        if (mVerticalDivider != dividerVertical) {
            mVerticalDivider = dividerVertical;
        }

        if (mHorizontalDivider != dividerHorizontal) {
            mHorizontalDivider = dividerHorizontal;
        }

        if (null != mEasyDefaultItemDecoration) {
            mEasyDefaultItemDecoration.setVerticalDividerDrawable(mVerticalDivider);
            mEasyDefaultItemDecoration.setHorizontalDividerDrawable(mHorizontalDivider);

            if (null != mWrapEasyRecyclerViewAdapter) {
                mWrapEasyRecyclerViewAdapter.notifyDataSetChanged();
            }
        }
    }

    public void setDividerVertical(Drawable dividerVertical) {
        if (!isDefaultItemDecoration || mVerticalDividerHeight <= 0) return;

        if (mVerticalDivider != dividerVertical) {
            mVerticalDivider = dividerVertical;
        }

        if (null != mEasyDefaultItemDecoration) {
            mEasyDefaultItemDecoration.setVerticalDividerDrawable(mVerticalDivider);

            if (null != mWrapEasyRecyclerViewAdapter) {
                mWrapEasyRecyclerViewAdapter.notifyDataSetChanged();
            }
        }
    }

    public void setDividerHorizontal(Drawable dividerHorizontal) {
        if (!isDefaultItemDecoration || mHorizontalDividerHeight <= 0) return;

        if (mHorizontalDivider != dividerHorizontal) {
            mHorizontalDivider = dividerHorizontal;
        }

        if (null != mEasyDefaultItemDecoration) {
            mEasyDefaultItemDecoration.setHorizontalDividerDrawable(mHorizontalDivider);

            if (null != mWrapEasyRecyclerViewAdapter) {
                mWrapEasyRecyclerViewAdapter.notifyDataSetChanged();
            }
        }
    }

    public void setDividerHeight(int height) {
        if (!isDefaultItemDecoration) return;

        mVerticalDividerHeight = height;
        mHorizontalDividerHeight = height;

        if (null != mEasyDefaultItemDecoration) {
            mEasyDefaultItemDecoration.setVerticalDividerDrawableHeight(mVerticalDividerHeight);
            mEasyDefaultItemDecoration.setHorizontalDividerDrawableHeight(mHorizontalDividerHeight);

            if (null != mWrapEasyRecyclerViewAdapter) {
                mWrapEasyRecyclerViewAdapter.notifyDataSetChanged();
            }
        }
    }

    public void setDividerVerticalHeight(int height) {
        if (!isDefaultItemDecoration) return;

        mVerticalDividerHeight = height;

        if (null != mEasyDefaultItemDecoration) {
            mEasyDefaultItemDecoration.setVerticalDividerDrawableHeight(mVerticalDividerHeight);

            if (null != mWrapEasyRecyclerViewAdapter) {
                mWrapEasyRecyclerViewAdapter.notifyDataSetChanged();
            }
        }
    }

    public void setDividerHorizontalHeight(int height) {
        if (!isDefaultItemDecoration) return;

        mHorizontalDividerHeight = height;

        if (null != mEasyDefaultItemDecoration) {
            mEasyDefaultItemDecoration.setHorizontalDividerDrawableHeight(mHorizontalDividerHeight);

            if (null != mWrapEasyRecyclerViewAdapter) {
                mWrapEasyRecyclerViewAdapter.notifyDataSetChanged();
            }
        }
    }

    public void setItemViewBothSidesMargin(int bothSidesMargin) {
        if (!isDefaultItemDecoration) return;

        mItemViewBothSidesMargin = bothSidesMargin;

        if (null != mEasyDefaultItemDecoration) {
            mEasyDefaultItemDecoration.setItemViewBothSidesMargin(mItemViewBothSidesMargin);

            if (null != mWrapEasyRecyclerViewAdapter) {
                mWrapEasyRecyclerViewAdapter.notifyDataSetChanged();
            }
        }
    }

    public void setDividerHorizontalLeftMargin(int margin) {
        if (!isDefaultItemDecoration) return;

        mHorizontalDividerLeftMargin = margin;

        if (null != mEasyDefaultItemDecoration) {
            mEasyDefaultItemDecoration.setHorizontalDividerLeftMargin(mHorizontalDividerLeftMargin);

            if (null != mWrapEasyRecyclerViewAdapter) {
                mWrapEasyRecyclerViewAdapter.notifyDataSetChanged();
            }
        }
    }

    public void setDividerHorizontalRightMargin(int margin) {
        if (!isDefaultItemDecoration) return;

        mHorizontalDividerRightMargin = margin;

        if (null != mEasyDefaultItemDecoration) {
            mEasyDefaultItemDecoration.setHorizontalDividerRightMargin(mHorizontalDividerRightMargin);

            if (null != mWrapEasyRecyclerViewAdapter) {
                mWrapEasyRecyclerViewAdapter.notifyDataSetChanged();
            }
        }
    }

    public void setDividerVerticalTopMargin(int margin) {
        if (!isDefaultItemDecoration) return;

        mVerticalDividerTopMargin = margin;

        if (null != mEasyDefaultItemDecoration) {
            mEasyDefaultItemDecoration.setVerticalDividerTopMargin(mVerticalDividerTopMargin);

            if (null != mWrapEasyRecyclerViewAdapter) {
                mWrapEasyRecyclerViewAdapter.notifyDataSetChanged();
            }
        }
    }

    public void setDividerVerticalBottomMargin(int margin) {
        if (!isDefaultItemDecoration) return;

        mVerticalDividerBottomMargin = margin;

        if (null != mEasyDefaultItemDecoration) {
            mEasyDefaultItemDecoration.setVerticalDividerBottomMargin(mVerticalDividerBottomMargin);

            if (null != mWrapEasyRecyclerViewAdapter) {
                mWrapEasyRecyclerViewAdapter.notifyDataSetChanged();
            }
        }
    }

    /**
     * HeadView onBindViewHolder callback
     *
     * @param onHeadViewBindViewHolderListener OnHeadViewBindViewHolderListener
     */
    public void setOnHeadViewBindViewHolderListener(EasyRecyclerView.OnHeadViewBindViewHolderListener onHeadViewBindViewHolderListener) {
        if (null != mWrapEasyRecyclerViewAdapter) {
            mWrapEasyRecyclerViewAdapter.setOnHeadViewBindViewHolderListener(onHeadViewBindViewHolderListener);
        } else {
            mTempOnHeadViewBindViewHolderListener = onHeadViewBindViewHolderListener;
        }
    }

    /**
     * FooterView onBindViewHolder callback
     *
     * @param onFooterViewBindViewHolderListener OnFooterViewBindViewHolderListener
     */
    public void setOnFooterViewBindViewHolderListener(EasyRecyclerView.OnFooterViewBindViewHolderListener onFooterViewBindViewHolderListener) {
        if (null != mWrapEasyRecyclerViewAdapter) {
            mWrapEasyRecyclerViewAdapter.setOnFooterViewBindViewHolderListener(onFooterViewBindViewHolderListener);
        } else {
            mTempOnFooterViewBindViewHolderListener = onFooterViewBindViewHolderListener;
        }
    }

    public void addHeaderView(View v) {
        addHeaderView(v, false);
    }

    public void addHeaderView(View v, boolean isScrollTo) {
        if (mHeaderView.contains(v)) return;

        mHeaderView.add(v);
        if (null != mWrapEasyRecyclerViewAdapter) {
            int pos = mHeaderView.size() - 1;
            mWrapEasyRecyclerViewAdapter.notifyItemInserted(pos);

            if (isScrollTo) {
                scrollToPosition(pos);
            }
        }
    }

    public boolean removeHeaderView(View v) {
        if (!mHeaderView.contains(v)) return false;

        if (null != mWrapEasyRecyclerViewAdapter) {
            mWrapEasyRecyclerViewAdapter.notifyItemRemoved(mHeaderView.indexOf(v));
        }
        return mHeaderView.remove(v);
    }

    public void addFooterView(View v) {
        addFooterView(v, false);
    }

    public void addFooterView(View v, boolean isScrollTo) {
        if (mFooterView.contains(v)) return;

        mFooterView.add(v);
        if (null != mWrapEasyRecyclerViewAdapter) {
            int pos = (null == mReqAdapter ? 0 : mReqAdapter.getItemCount()) + getHeaderViewsCount() + mFooterView.size() - 1;
            mWrapEasyRecyclerViewAdapter.notifyItemInserted(pos);
            if (isScrollTo) {
                scrollToPosition(pos);
            }
        }
    }

    public boolean removeFooterView(View v) {
        if (!mFooterView.contains(v)) return false;

        if (null != mWrapEasyRecyclerViewAdapter) {
            int pos = (null == mReqAdapter ? 0 : mReqAdapter.getItemCount()) + getHeaderViewsCount() + mFooterView.indexOf(v);
            mWrapEasyRecyclerViewAdapter.notifyItemRemoved(pos);
        }
        return mFooterView.remove(v);
    }

    public int getHeaderViewsCount() {
        return mHeaderView.size();
    }

    public int getFooterViewsCount() {
        return mFooterView.size();
    }

    public int getFirstVisiblePosition() {
        LayoutManager layoutManager = getLayoutManager();

        if (null == layoutManager) return 0;

        int ret = -1;

        switch (mLayoutManagerType) {
            case LAYOUT_MANAGER_TYPE_LINEAR:
                ret = ((LinearLayoutManager) layoutManager).findFirstCompletelyVisibleItemPosition() - getHeaderViewsCount();
                break;
            case LAYOUT_MANAGER_TYPE_GRID:
                ret = ((GridLayoutManager) layoutManager).findFirstCompletelyVisibleItemPosition() - getHeaderViewsCount();
                break;
            case LAYOUT_MANAGER_TYPE_STAGGERED_GRID:
                StaggeredGridLayoutManager tempStaggeredGridLayoutManager = (StaggeredGridLayoutManager) layoutManager;
                int[] firstVisibleItemPositions = new int[tempStaggeredGridLayoutManager.getSpanCount()];
                tempStaggeredGridLayoutManager.findFirstCompletelyVisibleItemPositions(firstVisibleItemPositions);
                ret = firstVisibleItemPositions[0] - getHeaderViewsCount();
                break;
        }

        return ret < 0 ? 0 : ret;
    }

    public int getLastVisiblePosition() {
        LayoutManager layoutManager = getLayoutManager();
        if (null == layoutManager) return -1;

        int curItemCount = (null != mReqAdapter ? mReqAdapter.getItemCount() - 1 : 0);
        int ret = -1;

        switch (mLayoutManagerType) {
            case LAYOUT_MANAGER_TYPE_LINEAR:
                ret = ((LinearLayoutManager) layoutManager).findLastCompletelyVisibleItemPosition() - getHeaderViewsCount();
                if (ret > curItemCount) {
                    ret -= getFooterViewsCount();
                }
                break;
            case LAYOUT_MANAGER_TYPE_GRID:
                ret = ((GridLayoutManager) layoutManager).findLastCompletelyVisibleItemPosition() - getHeaderViewsCount();
                if (ret > curItemCount) {
                    ret -= getFooterViewsCount();
                }
                break;
            case LAYOUT_MANAGER_TYPE_STAGGERED_GRID:
                StaggeredGridLayoutManager tempStaggeredGridLayoutManager = (StaggeredGridLayoutManager) layoutManager;
                int[] lastVisibleItemPositions = new int[tempStaggeredGridLayoutManager.getSpanCount()];
                tempStaggeredGridLayoutManager.findLastCompletelyVisibleItemPositions(lastVisibleItemPositions);
                if (lastVisibleItemPositions.length > 0) {
                    int maxPos = lastVisibleItemPositions[0];
                    for (int curPos : lastVisibleItemPositions) {
                        if (curPos > maxPos) maxPos = curPos;
                    }
                    ret = maxPos - getHeaderViewsCount();
                    if (ret > curItemCount) {
                        ret -= getFooterViewsCount();
                    }
                }
                break;
        }

        return ret < 0 ? (null != mReqAdapter ? mReqAdapter.getItemCount() - 1 : 0) : ret;
    }

    public void setHeaderDividersEnabled(boolean isHeaderDividersEnabled) {
        this.isHeaderDividersEnabled = isHeaderDividersEnabled;
        if (isDefaultItemDecoration && null != mEasyDefaultItemDecoration) {
            mEasyDefaultItemDecoration.setHeaderDividersEnabled(isHeaderDividersEnabled);

            if (null != mWrapEasyRecyclerViewAdapter) {
                mWrapEasyRecyclerViewAdapter.notifyDataSetChanged();
            }
        }
    }

    public void setFooterDividersEnabled(boolean isFooterDividersEnabled) {
        this.isFooterDividersEnabled = isFooterDividersEnabled;
        if (isDefaultItemDecoration && null != mEasyDefaultItemDecoration) {
            mEasyDefaultItemDecoration.setFooterDividersEnabled(isFooterDividersEnabled);

            if (null != mWrapEasyRecyclerViewAdapter) {
                mWrapEasyRecyclerViewAdapter.notifyDataSetChanged();
            }
        }
    }

    public void setNotShowGridEndDivider(boolean isNotShowGridEndDivider) {
        this.isNotShowGridEndDivider = isNotShowGridEndDivider;
        if (isDefaultItemDecoration && null != mEasyDefaultItemDecoration) {
            mEasyDefaultItemDecoration.setNotShowGridEndDivider(isNotShowGridEndDivider);

            if (null != mWrapEasyRecyclerViewAdapter) {
                mWrapEasyRecyclerViewAdapter.notifyDataSetChanged();
            }
        }
    }

    public void removeDefaultItemDecoration() {
        // remove default ItemDecoration
        if (null != mEasyDefaultItemDecoration) {
            removeItemDecoration(mEasyDefaultItemDecoration);
            mEasyDefaultItemDecoration = null;
        }
        isDefaultItemDecoration = false;
    }

    public int getCurLayoutManagerType() {
        return mLayoutManagerType;
    }

    public void setOnItemClickListener(OnItemClickListener listener) {
        if (null == mWrapEasyRecyclerViewAdapter) {
            mTempOnItemClickListener = listener;
        } else {
            mWrapEasyRecyclerViewAdapter.setOnItemClickListener(listener);
        }
    }

    public void setOnItemLongClickListener(OnItemLongClickListener listener) {
        if (null == mWrapEasyRecyclerViewAdapter) {
            mTempOnItemLongClickListener = listener;
        } else {
            mWrapEasyRecyclerViewAdapter.setOnItemLongClickListener(listener);
        }
    }

    public interface OnItemClickListener {
        void onItemClick(EasyRecyclerView easyRecyclerView, View view, int position);
    }

    public interface OnItemLongClickListener {
        boolean onItemLongClick(EasyRecyclerView easyRecyclerView, View view, int position);
    }

    public interface OnHeadViewBindViewHolderListener {
        void onHeadViewBindViewHolder(RecyclerView.ViewHolder holder, int position, boolean isInitializeInvoke);
    }

    public interface OnFooterViewBindViewHolderListener {
        void onFooterViewBindViewHolder(RecyclerView.ViewHolder holder, int position, boolean isInitializeInvoke);
    }

    private AdapterDataObserver mReqAdapterDataObserver = new AdapterDataObserver() {
        @Override
        public void onChanged() {
            mWrapEasyRecyclerViewAdapter.notifyDataSetChanged();
            processEmptyView();
        }

        public void onItemRangeInserted(int positionStart, int itemCount) {
            mWrapEasyRecyclerViewAdapter.notifyItemInserted(getHeaderViewsCount() + positionStart);
            processEmptyView();
        }

        public void onItemRangeRemoved(int positionStart, int itemCount) {
            mWrapEasyRecyclerViewAdapter.notifyItemRemoved(getHeaderViewsCount() + positionStart);
            processEmptyView();
        }

        @Override
        public void onItemRangeChanged(int positionStart, int itemCount) {
            mWrapEasyRecyclerViewAdapter.notifyItemRangeChanged(getHeaderViewsCount() + positionStart, itemCount);
            processEmptyView();
        }

        @Override
        public void onItemRangeMoved(int fromPosition, int toPosition, int itemCount) {
            mWrapEasyRecyclerViewAdapter.notifyItemMoved(getHeaderViewsCount() + fromPosition, getHeaderViewsCount() + toPosition);
        }
    };

}